module Race (
    raceRespond
  , RaceInfo(..)
  , ClientMessage
  , joinMessage
  , pingMessage
  ) where

import GameInitModel
import qualified CarPositionsModel as CPM
import Data.Car

type ClientMessage = String
data RaceInfo = RaceInfo {trackData :: Maybe Race
                         ,myCar     :: Maybe CarData
                         ,curSpeed  :: Double
                         }

golden::Double
golden = 1.6180

maxCornerSpeed::Double
maxCornerSpeed = 0.454

maxSpeed::Double
maxSpeed = 0.9

joinMessage::String -> String -> ClientMessage
joinMessage botname botkey = "{\"msgType\":\"join\",\"data\":{\"name\":\"" ++ botname ++ "\",\"key\":\"" ++ botkey ++ "\"}}"

pingMessage::ClientMessage
pingMessage = "{\"msgType\":\"ping\",\"data\":{}}"

throttleMessage::Double -> ClientMessage
throttleMessage amount = "{\"msgType\":\"throttle\",\"data\":" ++ show amount ++ "}"

raceRespond::RaceInfo -> [CPM.CarPosition] -> (Double, [ClientMessage])
raceRespond r xs = do
  let s = curSpeed r
  case trackData r of
    Just ri -> case myCar r of
      Just car -> do  -- There's a race and a car, calculate away!
        let carName = Data.Car.name car
        let myPos = CPM.findCar carName xs
        case myPos of
          Just pos -> do
            let throttle = setThrottle r pos
            (throttle, [throttleMessage $ mkThrottle throttle])
          Nothing -> (s, [pingMessage])
      Nothing  -> (s, [pingMessage]) -- Just cause we don't know which one is us...
    Nothing     -> (s, [pingMessage])

mkThrottle::Double -> Double
mkThrottle prop | prop > 1.0 = 1.0
mkThrottle prop | prop < 0.0 = 0.0
mkThrottle prop = prop

setThrottle::RaceInfo -> CPM.CarPosition -> Double
setThrottle r p = case trackData r of
  Just td -> do
    let t = pieces $ track td
    let idx = CPM.pieceIndex $ CPM.piecePosition p
    let next = if length t - 1 >= idx + 3 then idx + 3 else 0
    let curPiece = bendOrStraight $ t !! idx
    let nexPiece = bendOrStraight $ t !! next
    case curPiece of
      Left (Bend _ _) -> maxCornerSpeed
      Right (Straight len) -> case nexPiece of
        Left (Bend _ _) -> do
          let d = CPM.inPieceDistance $ CPM.piecePosition p
          if pastHalf len d then maxCornerSpeed else max maxSpeed $ curSpeed r * golden
        Right (Straight _) -> maxSpeed
  Nothing -> 0.0

pastHalf::Double -> Double -> Bool
pastHalf measure distance = measure / 2 < distance
