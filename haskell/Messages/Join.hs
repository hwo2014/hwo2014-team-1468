{-# LANGUAGE DeriveGeneric, OverloadedStrings #-}
module Messages.Join where

-- base
import Data.Functor ((<$>))
import GHC.Generics (Generic)

-- aeson
import Data.Aeson (FromJSON(parseJSON), ToJSON(toJSON))

-- text
import Data.Text (Text)

-- this package
import Messages

data JoinData = JoinData
	{ name :: Text
	, key  :: Text
	} deriving (Eq, Read, Show, Generic)

instance FromJSON JoinData
instance ToJSON   JoinData

newtype JoinMessage = JoinMessage JoinData deriving (Eq, Read, Show)

instance Message JoinMessage where
	msgType _ = MsgType "join"
	msgJSON dat _ _ = JoinMessage <$> parseJSON dat
	jsonMsg (JoinMessage dat) = (toJSON dat, Nothing, Nothing)
