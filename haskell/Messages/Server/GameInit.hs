{-# LANGUAGE OverloadedStrings #-}
module Messages.Server.GameInit where

-- base
import Data.Functor ((<$>))

-- aeson
import Data.Aeson (FromJSON(parseJSON), ToJSON(toJSON))

-- this package
import GameInitModel
import Messages

newtype GameInitMessage = GameInitMessage GameInitData
	deriving ({-Eq, Read, -}Show)

instance Message GameInitMessage where
	msgType _ = MsgType "gameInit"
	msgJSON dat _ _ = GameInitMessage <$> parseJSON dat
	jsonMsg (GameInitMessage dat) = (undefined {-toJSON dat-}, Nothing, Nothing)
