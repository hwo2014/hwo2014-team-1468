{-# LANGUAGE OverloadedStrings #-}
module Messages.Server.YourCar where

-- base
import Data.Functor ((<$>))

-- aeson
import Data.Aeson (FromJSON(parseJSON), ToJSON(toJSON))

-- this package
import Messages
import Data.Car

newtype YourCarMessage = YourCarMessage
	{ yourCar :: CarData
	} deriving (Eq, Read, Show)

instance Message YourCarMessage where
	msgType _ = MsgType "yourCar"
	msgJSON dat _ _ = YourCarMessage <$> parseJSON dat
	jsonMsg (YourCarMessage dat) = (toJSON dat, Nothing, Nothing)
