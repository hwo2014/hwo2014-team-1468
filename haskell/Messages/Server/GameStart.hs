{-# LANGUAGE DeriveGeneric, OverloadedStrings #-}
module Messages.Server.GameStart where

-- aeson
import Data.Aeson (Value(Null))

-- this package
import Messages

newtype GameStartMessage = GameStartMessage
	{ gameTick :: GameTick
	} deriving (Eq, Read, Show)

instance Message GameStartMessage where
	msgType _ = MsgType "gameStart"
	msgJSON _ _ (Just tick) = return $ GameStartMessage tick
	msgJSON _ _  Nothing    = fail "gameTick missing."
	jsonMsg msg = (Null, Nothing, Just $ gameTick msg)

