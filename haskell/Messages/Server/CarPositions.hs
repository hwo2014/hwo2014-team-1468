{-# LANGUAGE OverloadedStrings #-}
module Messages.Server.CarPositions where

-- base
import Data.Functor ((<$>))

-- aeson
import Data.Aeson (FromJSON(parseJSON), ToJSON(toJSON))

-- vector
import Data.Vector (Vector)

-- this package
import CarPositionsModel
import Messages

data CarPositionsMessage = CarPositionsMessage
	{ positions :: Vector CarPosition
	, gameId    :: GameId
	, gameTick  :: Maybe GameTick
	} deriving ({-Eq, Read, -}Show)

instance Message CarPositionsMessage where
	msgType _ = MsgType "carPositions"
	msgJSON dat (Just gid) tick =
		(\x -> CarPositionsMessage x gid tick) <$> parseJSON dat
	msgJSON _ Nothing _ = fail "Missing gameId."
	jsonMsg msg = (undefined {-toJSON $ positions msg-}, Just $ gameId msg, gameTick msg)

