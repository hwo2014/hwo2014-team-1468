{-# LANGUAGE OverloadedStrings #-}
module Messages.Server.Spawn where

-- base
import Data.Functor ((<$>))

-- aeson
import Data.Aeson (FromJSON(parseJSON), ToJSON(toJSON))

-- this package
import Messages
import Data.Car

data SpawnMessage = SpawnMessage
	{ car      :: CarData
	, gameId   :: GameId
	, gameTick :: GameTick
	} deriving (Eq, Read, Show)

instance Message SpawnMessage where
	msgType _ = MsgType "spawn"
	msgJSON dat (Just gid) (Just tick) =
		(\x -> SpawnMessage x gid tick) <$> parseJSON dat
	msgJSON _ Nothing _ = fail "Missing gameId."
	msgJSON _ _ Nothing = fail "Missing gameTick."
	jsonMsg msg = (toJSON $ car msg, Just $ gameId msg, Just $ gameTick msg)

