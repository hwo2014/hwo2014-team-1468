{-# LANGUAGE DeriveGeneric, OverloadedStrings #-}
module Messages.Server.GameEnd where

-- base
import Data.Functor ((<$>))
import GHC.Generics (Generic)

-- aeson
import Data.Aeson (FromJSON(parseJSON), ToJSON(toJSON))

-- vector
import Data.Vector (Vector)

-- this package
import Messages
import Data.Result
import Data.BestLap

data GameEndData = GameEndData
	{ results  :: Vector Result
	, bestLaps :: Vector BestLap
	} deriving (Eq, Read, Show, Generic)

instance FromJSON GameEndData
instance ToJSON   GameEndData

newtype GameEndMessage = GameEndMessage GameEndData deriving (Eq, Read, Show)

instance Message GameEndMessage where
	msgType _ = MsgType "gameEnd"
	msgJSON dat _ _ = GameEndMessage <$> parseJSON dat
	jsonMsg (GameEndMessage dat) = (toJSON dat, Nothing, Nothing)

