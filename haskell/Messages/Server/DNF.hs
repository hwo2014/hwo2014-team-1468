{-# LANGUAGE DeriveGeneric, OverloadedStrings #-}
module Messages.Server.DNF where

-- base
import Data.Functor ((<$>))
import GHC.Generics (Generic)

-- aeson
import Data.Aeson (FromJSON(parseJSON), ToJSON(toJSON))

-- text
import Data.Text (Text)

-- this package
import Messages
import Data.Car

data DNFData = DNFData
	{ car    :: CarData
	, reason :: Text
	} deriving (Eq, Read, Show, Generic)

instance FromJSON DNFData
instance ToJSON   DNFData

data DNFMessage = DNFMessage
	{ dat      :: DNFData
	, gameId   :: GameId
	, gameTick :: GameTick
	} deriving (Eq, Read, Show)

instance Message DNFMessage where
	msgType _ = MsgType "dnf"
	msgJSON idat (Just gid) (Just tick) =
		(\x -> DNFMessage x gid tick) <$> parseJSON idat
	msgJSON _ Nothing _ = fail "Missing gameId."
	msgJSON _ _ Nothing = fail "Missing gameTick."
	jsonMsg msg = (toJSON $ dat msg, Just $ gameId msg, Just $ gameTick msg)

