{-# LANGUAGE DeriveGeneric, OverloadedStrings #-}
module Messages.Server.LapFinished where

-- base
import Data.Functor ((<$>))
import GHC.Generics (Generic)

-- aeson
import Data.Aeson (FromJSON(parseJSON), ToJSON(toJSON))

-- this package
import Messages
import Data.Car
import Data.LapTime
import Data.RaceTime

data LapFinishedData = LapFinishedData
	{ car      :: CarData
	, lapTime  :: LapTime
	, raceTime :: RaceTime
	, ranking  :: Ranking
	} deriving (Eq, Read, Show, Generic)

instance FromJSON LapFinishedData
instance ToJSON   LapFinishedData

data Ranking = Ranking
	{ overall    :: Int
	, fastestLap :: Int
	} deriving (Eq, Read, Show, Generic)

instance FromJSON Ranking
instance ToJSON   Ranking

data LapFinishedMessage = LapFinishedMessage
	{ dat      :: LapFinishedData
	, gameId   :: GameId
	, gameTick :: GameTick
	} deriving (Eq, Read, Show)

instance Message LapFinishedMessage where
	msgType _ = MsgType "lapFinished"
	msgJSON idat (Just gid) (Just tick) =
		(\x -> LapFinishedMessage x gid tick) <$> parseJSON idat
	msgJSON _ Nothing _ = fail "Missing gameId."
	msgJSON _ _ Nothing = fail "Missing gameTick."
	jsonMsg msg = (toJSON $ dat msg, Just $ gameId msg, Just $ gameTick msg)


