{-# LANGUAGE DeriveGeneric, OverloadedStrings #-}
module Messages.Server.TurboAvailable where

-- base
import Data.Functor ((<$>))
import GHC.Generics (Generic)

-- aeson
import Data.Aeson (FromJSON(parseJSON), ToJSON(toJSON))

-- this package
import Messages

data TurboAvailableData = TurboAvailableData
	{ turboDurationMilliseconds :: Double
	, turboDurationTicks        :: Int
	, turboFactor               :: Double
	} deriving (Eq, Read, Show, Generic)

instance FromJSON TurboAvailableData
instance ToJSON   TurboAvailableData

newtype TurboAvailableMessage = TurboAvailableMessage TurboAvailableData
	deriving (Eq, Read, Show)

instance Message TurboAvailableMessage where
	msgType _ = MsgType "turboAvailable"
	msgJSON dat _ _ = TurboAvailableMessage <$> parseJSON dat
	jsonMsg (TurboAvailableMessage dat) = (toJSON dat, Nothing, Nothing)

