{-# LANGUAGE OverloadedStrings #-}
module Messages.Server.Crash where

-- base
import Data.Functor ((<$>))

-- aeson
import Data.Aeson (FromJSON(parseJSON), ToJSON(toJSON))

-- this package
import Messages
import Data.Car

data CrashMessage = CrashMessage
	{ car      :: CarData
	, gameId   :: GameId
	, gameTick :: GameTick
	} deriving (Eq, Read, Show)

instance Message CrashMessage where
	msgType _ = MsgType "crash"
	msgJSON dat (Just gid) (Just tick) =
		(\x -> CrashMessage x gid tick) <$> parseJSON dat
	msgJSON _ Nothing _ = fail "Missing gameId."
	msgJSON _ _ Nothing = fail "Missing gameTick."
	jsonMsg msg = (toJSON $ car msg, Just $ gameId msg, Just $ gameTick msg)
