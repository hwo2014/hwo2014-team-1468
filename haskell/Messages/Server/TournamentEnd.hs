{-# LANGUAGE OverloadedStrings #-}
module Messages.Server.TournamentEnd where

-- aeson
import Data.Aeson (Value(Null))

-- this package
import Messages

data TournamentEndMessage = TournamentEndMessage deriving (Eq, Read, Show)

instance Message TournamentEndMessage where
	msgType _ = MsgType "tournamentEnd"
	msgJSON _ _ _ = return TournamentEndMessage
	jsonMsg _ = (Null, Nothing, Nothing)


