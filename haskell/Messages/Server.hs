module Messages.Server where

-- base
import System.IO (Handle)

-- bytestring
import Data.ByteString.Char8 (hGetLine)

-- this package
import Messages
import Messages.Join
import Messages.Server.YourCar
import Messages.Server.GameInit
import Messages.Server.GameStart
import Messages.Server.CarPositions
import Messages.Server.GameEnd
import Messages.Server.TournamentEnd
import Messages.Server.Crash
import Messages.Server.Spawn
import Messages.Server.LapFinished
import Messages.Server.DNF
import Messages.Server.Finish
import Messages.Server.TurboAvailable

data ServerMessage = Join JoinMessage
	| YourCar YourCarMessage
	| GameInit GameInitMessage
	| GameStart GameStartMessage
	| CarPositions CarPositionsMessage
	| GameEnd GameEndMessage
	| TournamentEnd TournamentEndMessage
	| Crash CrashMessage
	| Spawn SpawnMessage
	| LapFinished LapFinishedMessage
	| DNF DNFMessage
	| Finish FinishMessage
	| TurboAvailable TurboAvailableMessage
	| Unknown MsgType
	deriving (Show)

getServerMessage :: Handle -> IO (Either String ServerMessage)
getServerMessage h = do
	line <- hGetLine h
	return $ parseMessageLine line smc Unknown
 where
	smc =
		[ MessageExists Join
		, MessageExists YourCar
		, MessageExists GameInit
		, MessageExists GameStart
		, MessageExists CarPositions
		, MessageExists GameEnd
		, MessageExists TournamentEnd
		, MessageExists Crash
		, MessageExists Spawn
		, MessageExists LapFinished
		, MessageExists DNF
		, MessageExists Finish
		, MessageExists TurboAvailable
		]
