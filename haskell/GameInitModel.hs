{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE OverloadedStrings #-}

module GameInitModel where


-- base
import Data.Foldable (Foldable(fold))
import Data.List (sortBy)
import Data.Maybe (catMaybes)
import Data.Monoid (Monoid)
import Data.Ord (comparing)
import Control.Applicative ((<$>), (<*>), (<|>))

-- aeson
import Data.Aeson (Object, (.:), (.:?), (.!=), withObject, FromJSON(..))
import Data.Aeson.Types (Parser)

-- this package
import Data.Car

-- Dimension

data Dimension = Dimension {
  dimensionLength   :: Double,
  width             :: Double,
  guideFlagPosition :: Double
} deriving (Show)

instance FromJSON Dimension where
  parseJSON = withObject "Dimension" $ \o ->
    Dimension       <$>
    (o .: "length") <*>
    (o .: "width")  <*>
    (o .: "guideFlagPosition")

-- Car

data Car = Car {
  carId      :: CarData,
  dimensions :: Dimension
} deriving (Show)

instance FromJSON Car where
  parseJSON = withObject "Car" $ \o ->
    Car         <$>
    (o .: "id") <*>
    (o .: "dimensions")

-- Lane

type LaneIndex = Int
data Lane = Lane {
  distanceFromCenter :: Int,
  index              :: LaneIndex
} deriving (Show)

instance FromJSON Lane where
  parseJSON = withObject "Lane" $ \o ->
    Lane                        <$>
    (o .: "distanceFromCenter") <*>
    (o .: "index")

-- Piece

data Straight = Straight { straightLength :: Double } deriving (Eq, Read, Show)
parseStraight :: Object -> Parser Straight
parseStraight o = Straight <$> o .: "length"

data Bend = Bend { radius :: Double, bendAngle :: Double }
	deriving (Eq, Read, Show)
parseBend :: Object -> Parser Bend
parseBend o = Bend <$> o .: "radius" <*> o .: "angle"

data Piece = Piece
	{ bendOrStraight :: Either Bend Straight
	, switch :: Bool
	, bridge :: Bool
	} deriving (Eq, Read, Show)

instance FromJSON Piece where
  parseJSON = withObject "Piece" $ \o -> Piece           <$>
    (Left <$> parseBend o <|> Right <$> parseStraight o) <*>
    (o .:? "switch" .!= False)                           <*>
    (o .:? "bridge" .!= False)

-- | Helper for 'laneLengthInPiece' and 'switchLengthInPiece'.
pieceLengthHelper :: Double -- ^ lane distance from center
                  -> Piece  -- ^ piece
                  -> Double -- ^ length
pieceLengthHelper lr Piece{bendOrStraight = p} = case p of
 Left  Bend{radius=r, bendAngle=deg} ->
	(r + lr * signum deg) * pi * abs deg / 180
 Right Straight{straightLength=l}    -> l

-- | Calculate linear distance traveled in a single lane
laneLengthInPiece :: Int    -- ^ lane distance from center
                  -> Piece  -- ^ piece
                  -> Double -- ^ length
laneLengthInPiece = pieceLengthHelper . fromIntegral

-- | Calcuate linear distance traveled when doing a switch.
switchLengthInPiece :: Int    -- ^ starting lane distance from center
                    -> Int    -- ^ ending lane distance from center
                    -> Piece  -- ^ switch piece
                    -> Double -- ^ length
switchLengthInPiece s e = pieceLengthHelper $ fromIntegral (s + e) / 2

-- StartingPoint

data StartingPoint = StartingPoint {
  position :: Position,
  angle    :: Double
} deriving (Show)

instance FromJSON StartingPoint where
  parseJSON = withObject "StartingPoint" $ \o ->
    StartingPoint     <$>
    (o .: "position") <*>
    (o .: "angle")

-- Position

data Position = Position {
  x :: Double,
  y :: Double
} deriving (Show)

instance FromJSON Position where
  parseJSON = withObject "Position" $ \o ->
    Position   <$>
    (o .: "x") <*>
    (o .: "y")

-- Track

data Track = Track {
  name          :: String,
  startingPoint :: StartingPoint,
  pieces        :: [Piece],
  lanes         :: [Lane]
} deriving (Show)

data Switch = SLeft | SRight deriving (Bounded, Enum, Eq, Read, Show)

data Tactic = Fast | Overtake deriving (Bounded, Enum, Eq, Read, Show)

-- |Implements the tactics.  Fast chooses the shortest line; Overtake chooses
-- the 2nd shortest.
tactic :: Ord weight
       => Maybe (cmd, weight) -- ^ left choice, if exists
       -> (cmd, weight)       -- ^ straight choice
       -> Maybe (cmd, weight) -- ^ right choice, if exists
       -> Tactic              -- ^ tactic to use
       -> (cmd, weight)       -- ^ lowest weight choice
tactic l s r t = case ordered of
	(f:o:_) -> case t of
	 Fast     -> f
	 Overtake -> o
	[o] -> o
	[]  -> error "Sort on non-empty list gave empty list!"
 where
	ordered = sortBy (comparing snd) $ catMaybes [l, Just s, r]

type PieceIndex = Int

-- Outer container parallel with pieces, inner container parallel with lanes
type Strategy = [[Tactic -> Maybe Switch]]

duplicateWith :: (Monoid a, Foldable t)
              => (Int -> a -> t a) -- ^ local replicate
              -> Int               -- ^ count
              -> a                 -- ^ example
              -> a                 -- ^ count copies of example
duplicateWith repl{-icate-} n = fold . repl n

-- |Pre-computes lane switching strategy
computeStrategy :: [Piece]  -- ^ Piece of track
                -> [Int]    -- ^ Lane distances from center
                -> Strategy -- ^ Switching strategy
computeStrategy lapPieces laneDistances =
	take (length lapPieces) . drop 1 . (map.map.fmap) fst $ scanr incStrategy initWeightedStrategy longRun
 where
	lrCnt = max 2 . subtract 1 $ length laneDistances
	-- Finite, but long repetition of a track, over which the strategy
	-- consolidates
	longRun :: [Piece]
	longRun = duplicateWith replicate lrCnt lapPieces
	-- Starter value for the fold; strategy for 0 pieces; container is parallel
	-- to lanes
	initWeightedStrategy :: [Tactic -> (Maybe Switch, Double)]
	initWeightedStrategy = fmap (const $ const (Nothing, 0)) laneDistances
	-- Lanes to the left of each lane
	leftDistances = init $ Nothing : straightDistances
	-- Lanes to the right of each lane (overlong)
	rightDistances = tail $ straightDistances ++ [Nothing]
	-- landDistances "lifted" to match type of leftDistances and rightDistances
	straightDistances = map Just laneDistances
	-- Prepend processing one piece to an existing strategy (fold funtion)
	incStrategy :: Piece -> [Tactic -> (Maybe Switch, Double)] -> [Tactic -> (Maybe Switch, Double)]
	incStrategy p strat = zipWith3 tactic leftOptions straightOptions rightOptions
	 where
		laneWeights = map (snd . ($ Fast)) strat
		leftOptions  = zipWith3 (switchOption SLeft)
		               laneDistances leftDistances laneWeights
		rightOptions = zipWith3 (switchOption SRight)
		               laneDistances rightDistances laneWeights
		straightOptions = zipWith straightOption
		                  laneDistances laneWeights
		straightOption ld r = (Nothing, laneLengthInPiece ld p + r)
		switchOption _ _ Nothing  _ = Nothing
		switchOption s sd (Just ed) r = Just (Just s, switchLengthInPiece sd ed p + r)

instance FromJSON Track where
  parseJSON = withObject "Track" $ \o ->
    Track                  <$>
    (o .: "name")          <*>
    (o .: "startingPoint") <*>
    (o .: "pieces")        <*>
    (o .: "lanes")

-- RaceSession

data RaceSession = RaceSession {
  laps :: Maybe Int,
  durationMs :: Maybe Int,
  maxLapTimeMs :: Maybe Int,
  quickRace :: Maybe Bool
} deriving (Show)

instance FromJSON RaceSession where
  parseJSON = withObject "RaceSession" $ \o ->
    RaceSession            <$>
    (o .:? "laps")         <*>
    (o .:? "durationMs")   <*>
    (o .:? "maxLapTimeMs") <*>
    (o .:? "quickRace")

-- Race

data Race = Race {
  track :: Track,
  cars  :: [Car],
  raceSession :: RaceSession
} deriving (Show)

instance FromJSON Race where
  parseJSON = withObject "Race" $ \o ->
    Race           <$>
    (o .: "track") <*>
    (o .: "cars")  <*>
    (o .: "raceSession")

-- GameInitData

data GameInitData = GameInitData {
  race :: Race
} deriving (Show)

instance FromJSON GameInitData where
  parseJSON = withObject "GameInitData" $ \o ->
    GameInitData <$>
    (o .: "race")

-- Helpers

players :: GameInitData -> [CarData]
players gameInit =
  map carId . cars $ race gameInit

piecesOfGame :: GameInitData -> [Piece]
piecesOfGame gameInit =
  pieces $ track $ race gameInit

lanesOfGame :: GameInitData -> [Lane]
lanesOfGame gameInit =
  lanes $ track $ race gameInit

reportGameInit :: GameInitData -> String
reportGameInit gameInit =
  "Players: " ++ show (players gameInit) ++ ", Track: " ++ show (length $ piecesOfGame gameInit) ++ " pieces"
