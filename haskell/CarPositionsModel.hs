{-# LANGUAGE OverloadedStrings #-}

module CarPositionsModel where

import Data.Aeson ((.:), FromJSON(..), withObject)
import Control.Applicative ((<$>), (<*>))

import Data.List

-- text
import Data.Text (Text)

-- this package
import Data.Car (CarData)
import qualified Data.Car as CD

-- CarLane

data CarLane = CarLane {
  startLaneIndex :: Int,
  endLaneIndex   :: Int
} deriving (Show)

instance FromJSON CarLane where
  parseJSON = withObject "CarLane" $ \o ->
    CarLane <$>
    (o .: "startLaneIndex") <*>
    (o .: "endLaneIndex")

-- PiecePosition

data PiecePosition = PiecePosition {
  pieceIndex      :: Int,
  inPieceDistance :: Double,
  lane            :: CarLane,
  lap             :: Int
} deriving (Show)

instance FromJSON PiecePosition where
  parseJSON = withObject "PiecePosition" $ \o ->
    PiecePosition            <$>
    (o .: "pieceIndex")      <*>
    (o .: "inPieceDistance") <*>
    (o .: "lane")            <*>
    (o .: "lap")

-- CarPosition

data CarPosition = CarPosition {
  carId         :: CarData,
  angle         :: Double,
  piecePosition :: PiecePosition
} deriving (Show)

instance FromJSON CarPosition where
  parseJSON = withObject "CarPosition" $ \o ->
    CarPosition    <$>
    (o .: "id")    <*>
    (o .: "angle") <*>
    (o .: "piecePosition")

-- Helpers

findCar :: Text -> [CarPosition] -> Maybe CarPosition
findCar carName = find nameMatches
  where nameMatches carPosition = CD.name (carId carPosition) == carName
