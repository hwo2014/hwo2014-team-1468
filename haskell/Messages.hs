{-# LANGUAGE DeriveFunctor, ExistentialQuantification,
             GeneralizedNewtypeDeriving, OverloadedStrings, RankNTypes #-}
module Messages where

-- base
import Data.Foldable (foldMap)
import Data.Functor ((<$>))
import Data.Maybe (catMaybes)
import Data.Monoid (First(First, getFirst), (<>))

--aeson
import Data.Aeson
	( FromJSON, ToJSON, Value(Object, String), Object
	, eitherDecodeStrict', object, withObject, (.=), (.:), (.:?)
	)
import Data.Aeson.Types (Parser, parseEither)

-- bytestring
import Data.ByteString (ByteString)

-- text
import           Data.Text (Text)
import qualified Data.Text as T

-- unordered-containers
import qualified Data.HashMap.Strict as HM

newtype MsgType = MsgType Text deriving (Eq, Read, Show, FromJSON, ToJSON)

newtype GameId = GameId Text deriving (Eq, Read, Show, FromJSON, ToJSON)

newtype GameTick = GameTick Int
	deriving (Eq, Ord, Read, Show, FromJSON, ToJSON)

class Message m where
	msgType :: p m -> MsgType
	msgJSON :: Value -> Maybe GameId -> Maybe GameTick -> Parser m
	jsonMsg :: m -> (Value, Maybe GameId, Maybe GameTick)

-- |Implementation of parseJSON from FromJSON for anthing that implements
-- Message.
messageParseJSON :: Message m => Value -> Parser m
messageParseJSON = r
 where
	emt = msgType $ r undefined
	emtStr = let MsgType txt = emt in T.unpack txt
	r = withObject emtStr $ \o -> do
		mt   <- o .: "msgType"
		let
			(MsgType txt) = mt
			mtStr = T.unpack txt
		if emt == mt
		 then withMessage msgJSON o
		 else fail $ "Got: " <> mtStr <> "; Expected: " <> emtStr

withMessage :: (Value -> Maybe GameId -> Maybe GameTick -> Parser a) -> Object -> Parser a
withMessage pf o = do
	dat  <- o .:  "data"
	gid  <- o .:? "gameId"
	tick <- o .:? "gameTick"
	pf dat gid tick

-- |Implementation of toJSON from ToJSON for anthing that implements Message.
messageToJSON :: Message m => m -> Value
messageToJSON m = object $ required ++ optional
 where
	(dat, gid, tick) = jsonMsg m
	required = [ "msgType" .= msgType [m], "data" .= dat ]
	optional = catMaybes
		[ ("gameId"   .=) <$> gid
		, ("gameTick" .=) <$> tick
		]

data SomeMessageConsumer a = forall m. Message m => MessageExists
	{ consume :: m -> a
	}

-- Should unknown hanler get the JSON object?
parseMessageLine :: ByteString              -- ^ bytes from server
                 -> [SomeMessageConsumer a] -- ^ consumers of differnt messages
                 -> (MsgType -> a)          -- ^ unknown message handler
                 -> Either String a         -- ^ error message or result
parseMessageLine line consumers unkMsg = do
	v <- eitherDecodeStrict' line
	case v of
	 Object o -> case HM.lookup "msgType" o of
		Just (String mtStr) ->
			let
				mt = MsgType mtStr
				mbParser = getFirst
				         $ foldMap (tryConsumer mt) consumers
			in case mbParser of
			 Nothing -> Right $ unkMsg mt
			 Just p  -> parseEither p o
		Just _  -> Left "msgType not a string."
		Nothing -> Left "No msgType in message."
	 _  -> Left "Top-level value not an object."
 where
	tryConsumer mt (MessageExists c) =
		First $ if emt == mt
		 then Just $ (fmap.fmap) c mp
		 else Nothing
	 where
		mp = withMessage msgJSON
		emt = msgType $ mp undefined
