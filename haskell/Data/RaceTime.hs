{-# LANGUAGE DeriveGeneric #-}
module Data.RaceTime where

-- base
import GHC.Generics (Generic)

-- aeson
import Data.Aeson (FromJSON, ToJSON)

data RaceTime = RaceTime
	{ laps   :: Int
	, ticks  :: Int
	, millis :: Int
	} deriving (Eq, Read, Show, Generic)

instance FromJSON RaceTime
instance ToJSON   RaceTime
