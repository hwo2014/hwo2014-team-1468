{-# LANGUAGE DeriveGeneric #-}
module Data.BestLap where

-- base
import GHC.Generics (Generic)

-- aeson
import Data.Aeson (FromJSON, ToJSON)

-- this package
import Data.Car
import Data.LapTime

data BestLap = BestLap
	{ car    :: CarData
	, result :: LapTime
	} deriving (Eq, Read, Show, Generic)

instance FromJSON BestLap
instance ToJSON   BestLap

