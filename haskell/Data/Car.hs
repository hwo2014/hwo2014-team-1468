{-# LANGUAGE DeriveGeneric #-}
module Data.Car where

-- base
import GHC.Generics (Generic)

-- aeson
import Data.Aeson (FromJSON, ToJSON)

-- text
import Data.Text (Text)

data CarData = CarData
	{ name  :: Text
	, color :: Text
	} deriving (Eq, Read, Show, Generic)

instance FromJSON CarData
instance ToJSON   CarData
