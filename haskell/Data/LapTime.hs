{-# LANGUAGE DeriveGeneric #-}
module Data.LapTime where

-- base
import GHC.Generics (Generic)

-- aeson
import Data.Aeson (FromJSON, ToJSON)

data LapTime = LapTime
	{ lap    :: Int
	, ticks  :: Int
	, millis :: Int
	} deriving (Eq, Read, Show, Generic)

instance FromJSON LapTime
instance ToJSON   LapTime
