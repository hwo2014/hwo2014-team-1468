{-# LANGUAGE DeriveGeneric #-}
module Data.Result where

-- base
import GHC.Generics (Generic)

-- aeson
import Data.Aeson (FromJSON, ToJSON)

-- this package
import Data.Car
import Data.RaceTime

data Result = Result
	{ car    :: CarData
	, result :: RaceTime
	} deriving (Eq, Read, Show, Generic)

instance FromJSON Result
instance ToJSON   Result

