{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}

module Main where

import System.Environment (getArgs)
import System.Exit (exitFailure)

import Network(connectTo, PortID(..))
import System.IO(hPutStrLn, hSetBuffering, BufferMode(..), Handle)
import Control.Monad
import qualified Data.Vector as V

import Messages.Server
import Messages.Server.GameInit (GameInitMessage(GameInitMessage))
import qualified Messages.Server.YourCar as YC
import qualified Messages.Server.CarPositions as CP
import GameInitModel
import Race

connectToServer::String -> String -> IO Handle
connectToServer server port = connectTo server . PortNumber $ fromIntegral (read port :: Integer)

main :: IO ()
main = do
  args <- getArgs
  case args of
    [server, port, botname, botkey] ->
      run server port botname botkey
    _                               -> do
      putStrLn "Usage: hwo2014bot <host> <port> <botname> <botkey>"
      exitFailure

run::String -> String -> String -> String -> IO ()
run server port botname botkey = do
  h <- connectToServer server port
  hSetBuffering h LineBuffering
  hPutStrLn h $ joinMessage botname botkey
  connectedLoop h botname Nothing

connectedLoop::Handle -> String -> Maybe RaceInfo -> IO ()
connectedLoop h botname mrace = do
  emsg <- getServerMessage h
  case emsg of
   Left err -> putStrLn err
   Right msg -> handleServerMessage h msg botname mrace

handleServerMessage :: Handle -> ServerMessage -> String -> Maybe RaceInfo -> IO ()
handleServerMessage h serverMessage botname mrace = do
  (newRace, responses) <- respond serverMessage botname mrace
  forM_ responses $ hPutStrLn h
  connectedLoop h botname newRace

respond :: ServerMessage -> String -> Maybe RaceInfo -> IO (Maybe RaceInfo, [ClientMessage])
respond message _ r = case message of
  Join{}                    -> do
    putStrLn "Joined"
    return (Nothing, [])
  YourCar yourCarMsg           -> do
    putStrLn "Got a yourCar message"
    let car = Just $ YC.yourCar yourCarMsg
    case r of
      Just ri -> do
        let raceTrack= trackData ri
        let info = RaceInfo raceTrack car 0.0
        return (Just info, [])
      Nothing   -> return (Just $ RaceInfo Nothing car 0.0, [pingMessage])
  GameInit gameInitMsg         -> do
    let
      GameInitMessage gameInit = gameInitMsg
      rd = Just . race $ gameInit
    case r of
      Just ri -> do
        let me = myCar ri
        putStrLn $ "GameInit: " ++ reportGameInit gameInit
        return (Just $ RaceInfo rd me 0.0, [pingMessage])
      Nothing -> do
        putStrLn $ "GameInit: " ++ reportGameInit gameInit
        return (Just $ RaceInfo rd Nothing 0.0, [pingMessage])
  CarPositions cpMsg -> case r of
    Just ri -> do
      let raceTrack = trackData ri
      let car = myCar ri
      let (throttle, msgs) = raceRespond ri . V.toList $ CP.positions cpMsg
      let newRI = RaceInfo raceTrack car throttle
      return (Just newRI, msgs)
    Nothing -> return (r, [pingMessage])
  TurboAvailable{}            -> do
    putStrLn "Turbo Available"
    return (r, [pingMessage])
  _                         ->
    unknownHandler message r

unknownHandler::ServerMessage -> Maybe RaceInfo -> IO (Maybe RaceInfo, [ClientMessage])
unknownHandler sm r = do
    putStrLn $ "Unknown message: " ++ show sm
    return (r, [])
